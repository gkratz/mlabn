// Generated by using Rcpp::compileAttributes() -> do not edit by hand
// Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#include <RcppArmadillo.h>
#include <Rcpp.h>

using namespace Rcpp;

// irls_binomial_cpp_br
Rcpp::List irls_binomial_cpp_br(arma::mat A, arma::vec b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_binomial_cpp_br(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::vec >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_binomial_cpp_br(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}
// irls_binomial_cpp_fast_br
Rcpp::List irls_binomial_cpp_fast_br(arma::mat A, arma::vec b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_binomial_cpp_fast_br(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::vec >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_binomial_cpp_fast_br(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}
// irls_binomial_cpp_fast
Rcpp::List irls_binomial_cpp_fast(arma::mat A, arma::vec b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_binomial_cpp_fast(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::vec >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_binomial_cpp_fast(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}
// irls_binomial_cpp
Rcpp::List irls_binomial_cpp(arma::mat A, arma::vec b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_binomial_cpp(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::vec >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_binomial_cpp(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}
// irls_gaussian_cpp_fast
Rcpp::List irls_gaussian_cpp_fast(arma::mat A, arma::vec b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_gaussian_cpp_fast(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::vec >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_gaussian_cpp_fast(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}
// irls_gaussian_cpp
Rcpp::List irls_gaussian_cpp(arma::mat A, arma::mat b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_gaussian_cpp(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::mat >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_gaussian_cpp(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}
// factorial_fast
int factorial_fast(int n);
RcppExport SEXP _mlabn_factorial_fast(SEXP nSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< int >::type n(nSEXP);
    rcpp_result_gen = Rcpp::wrap(factorial_fast(n));
    return rcpp_result_gen;
END_RCPP
}
// irls_poisson_cpp_fast
Rcpp::List irls_poisson_cpp_fast(arma::mat A, arma::vec b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_poisson_cpp_fast(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::vec >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_poisson_cpp_fast(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}
// factorial
int factorial(int n);
RcppExport SEXP _mlabn_factorial(SEXP nSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< int >::type n(nSEXP);
    rcpp_result_gen = Rcpp::wrap(factorial(n));
    return rcpp_result_gen;
END_RCPP
}
// irls_poisson_cpp
Rcpp::List irls_poisson_cpp(arma::mat A, arma::vec b, double maxit, double tol);
RcppExport SEXP _mlabn_irls_poisson_cpp(SEXP ASEXP, SEXP bSEXP, SEXP maxitSEXP, SEXP tolSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< arma::mat >::type A(ASEXP);
    Rcpp::traits::input_parameter< arma::vec >::type b(bSEXP);
    Rcpp::traits::input_parameter< double >::type maxit(maxitSEXP);
    Rcpp::traits::input_parameter< double >::type tol(tolSEXP);
    rcpp_result_gen = Rcpp::wrap(irls_poisson_cpp(A, b, maxit, tol));
    return rcpp_result_gen;
END_RCPP
}

static const R_CallMethodDef CallEntries[] = {
    {"_mlabn_irls_binomial_cpp_br", (DL_FUNC) &_mlabn_irls_binomial_cpp_br, 4},
    {"_mlabn_irls_binomial_cpp_fast_br", (DL_FUNC) &_mlabn_irls_binomial_cpp_fast_br, 4},
    {"_mlabn_irls_binomial_cpp_fast", (DL_FUNC) &_mlabn_irls_binomial_cpp_fast, 4},
    {"_mlabn_irls_binomial_cpp", (DL_FUNC) &_mlabn_irls_binomial_cpp, 4},
    {"_mlabn_irls_gaussian_cpp_fast", (DL_FUNC) &_mlabn_irls_gaussian_cpp_fast, 4},
    {"_mlabn_irls_gaussian_cpp", (DL_FUNC) &_mlabn_irls_gaussian_cpp, 4},
    {"_mlabn_factorial_fast", (DL_FUNC) &_mlabn_factorial_fast, 1},
    {"_mlabn_irls_poisson_cpp_fast", (DL_FUNC) &_mlabn_irls_poisson_cpp_fast, 4},
    {"_mlabn_factorial", (DL_FUNC) &_mlabn_factorial, 1},
    {"_mlabn_irls_poisson_cpp", (DL_FUNC) &_mlabn_irls_poisson_cpp, 4},
    {NULL, NULL, 0}
};

RcppExport void R_init_mlabn(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
